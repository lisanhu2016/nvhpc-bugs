#include <stdio.h>
#include <stdlib.h>

typedef struct mstring_t {
    size_t l;
    char *s;
} mstring_t;

int main() {
    mstring_t *refs = (mstring_t *)calloc(1, sizeof(mstring_t));
    refs[0].s = calloc(10, sizeof(char));
    refs[0].s[0] = 'a';

#pragma acc parallel loop copyin(refs[:1])
    for (size_t i = 0; i < 1; i++) {
        printf("refs[0].s[0] = %c\n", refs[i].s[0]);
    }

    free(refs[0].s);
    free(refs);
    return 0;
}

#   Introduction

This bug happens when trying to compile the code using NVHPC/20.7

##  Steps to reproduce
```
mkdir build
cd build
cmake ../
make
```

##  Observations
*   This bug happens for both CPU and GPU code
*   This bug only needs the `-acc` flag to trigger

##  System information
* OS: Ubuntu 18.04
* gcc: 7.5.0
* nvhpc: 20.7
* CUDA: 11.0

##  Console output
```bash
(base) lisanhu@skywalker:~/workspace/nvhpc-bugs/deep-data-access-NVVM_ERROR_COMPILATION
$ ls
bug.c  bug.cc  CMakeLists.txt  README.md
(base) lisanhu@skywalker:~/workspace/nvhpc-bugs/deep-data-access-NVVM_ERROR_COMPILATION
$ mkdir build
(base) lisanhu@skywalker:~/workspace/nvhpc-bugs/deep-data-access-NVVM_ERROR_COMPILATION
$ cd build
(base) lisanhu@skywalker:~/workspace/nvhpc-bugs/deep-data-access-NVVM_ERROR_COMPILATION/build
$ cmake ../
-- The C compiler identification is PGI 20.7.0
-- The CXX compiler identification is PGI 20.7.0
-- Check for working C compiler: /opt/nvidia/hpc/20.7/Linux_x86_64/20.7/compilers/bin/nvc
-- Check for working C compiler: /opt/nvidia/hpc/20.7/Linux_x86_64/20.7/compilers/bin/nvc -- works
-- Detecting C compiler ABI info
-- Detecting C compiler ABI info - done
-- Detecting C compile features
-- Detecting C compile features - done
-- Check for working CXX compiler: /opt/nvidia/hpc/20.7/Linux_x86_64/20.7/compilers/bin/nvc++
-- Check for working CXX compiler: /opt/nvidia/hpc/20.7/Linux_x86_64/20.7/compilers/bin/nvc++ -- works
-- Detecting CXX compiler ABI info
-- Detecting CXX compiler ABI info - done
-- Detecting CXX compile features
-- Detecting CXX compile features - done
-- Configuring done
-- Generating done
-- Build files have been written to: /usa/lisanhu/workspace/nvhpc-bugs/deep-data-access-NVVM_ERROR_COMPILATION/build
(base) lisanhu@skywalker:~/workspace/nvhpc-bugs/deep-data-access-NVVM_ERROR_COMPILATION/build
$ make bug-cc
/usa/lisanhu/mine/libs/cmake/bin/cmake -S/usa/lisanhu/workspace/nvhpc-bugs/deep-data-access-NVVM_ERROR_COMPILATION -B/usa/lisanhu/workspace/nvhpc-bugs/deep-data-access-NVVM_ERROR_COMPILATION/build --check-build-system CMakeFiles/Makefile.cmake 0
make -f CMakeFiles/Makefile2 bug-cc
make[1]: Entering directory '/usa/lisanhu/workspace/nvhpc-bugs/deep-data-access-NVVM_ERROR_COMPILATION/build'
/usa/lisanhu/mine/libs/cmake/bin/cmake -S/usa/lisanhu/workspace/nvhpc-bugs/deep-data-access-NVVM_ERROR_COMPILATION -B/usa/lisanhu/workspace/nvhpc-bugs/deep-data-access-NVVM_ERROR_COMPILATION/build --check-build-system CMakeFiles/Makefile.cmake 0
/usa/lisanhu/mine/libs/cmake/bin/cmake -E cmake_progress_start /usa/lisanhu/workspace/nvhpc-bugs/deep-data-access-NVVM_ERROR_COMPILATION/build/CMakeFiles 2
make -f CMakeFiles/Makefile2 CMakeFiles/bug-cc.dir/all
make[2]: Entering directory '/usa/lisanhu/workspace/nvhpc-bugs/deep-data-access-NVVM_ERROR_COMPILATION/build'
make -f CMakeFiles/bug-cc.dir/build.make CMakeFiles/bug-cc.dir/depend
make[3]: Entering directory '/usa/lisanhu/workspace/nvhpc-bugs/deep-data-access-NVVM_ERROR_COMPILATION/build'
cd /usa/lisanhu/workspace/nvhpc-bugs/deep-data-access-NVVM_ERROR_COMPILATION/build && /usa/lisanhu/mine/libs/cmake/bin/cmake -E cmake_depends "Unix Makefiles" /usa/lisanhu/workspace/nvhpc-bugs/deep-data-access-NVVM_ERROR_COMPILATION /usa/lisanhu/workspace/nvhpc-bugs/deep-data-access-NVVM_ERROR_COMPILATION /usa/lisanhu/workspace/nvhpc-bugs/deep-data-access-NVVM_ERROR_COMPILATION/build /usa/lisanhu/workspace/nvhpc-bugs/deep-data-access-NVVM_ERROR_COMPILATION/build /usa/lisanhu/workspace/nvhpc-bugs/deep-data-access-NVVM_ERROR_COMPILATION/build/CMakeFiles/bug-cc.dir/DependInfo.cmake --color=
Scanning dependencies of target bug-cc
make[3]: Leaving directory '/usa/lisanhu/workspace/nvhpc-bugs/deep-data-access-NVVM_ERROR_COMPILATION/build'
make -f CMakeFiles/bug-cc.dir/build.make CMakeFiles/bug-cc.dir/build
make[3]: Entering directory '/usa/lisanhu/workspace/nvhpc-bugs/deep-data-access-NVVM_ERROR_COMPILATION/build'
[ 50%] Building CXX object CMakeFiles/bug-cc.dir/bug.cc.o
/opt/nvidia/hpc/20.7/Linux_x86_64/20.7/compilers/bin/nvc++    -g -O0   -fPIC -acc -Minline -Minfo=accel -gpu=nordc -o CMakeFiles/bug-cc.dir/bug.cc.o -c /usa/lisanhu/workspace/nvhpc-bugs/deep-data-access-NVVM_ERROR_COMPILATION/bug.cc
main:
     11, Generating copyin(refs[:1]) [if not already present]
         Generating Tesla code
         14, #pragma acc loop gang /* blockIdx.x threadIdx.x */
nvvmCompileProgram error 9: NVVM_ERROR_COMPILATION.
Error: /tmp/pgaccNdLh1dUK3kAR.gpu (204, 10): parse stored value and pointer type do not match
ptxas /tmp/pgaccxdLhf2s1NJib.ptx, line 1; fatal   : Missing .version directive at start of file '/tmp/pgaccxdLhf2s1NJib.ptx'
ptxas fatal   : Ptx assembly aborted due to errors
NVC++-F-0155-Compiler failed to translate accelerator region (see -Minfo messages): Device compiler exited with error status code (/usa/lisanhu/workspace/nvhpc-bugs/deep-data-access-NVVM_ERROR_COMPILATION/bug.cc: 11)
NVC++/x86-64 Linux 20.7-0: compilation aborted
CMakeFiles/bug-cc.dir/build.make:65: recipe for target 'CMakeFiles/bug-cc.dir/bug.cc.o' failed
make[3]: *** [CMakeFiles/bug-cc.dir/bug.cc.o] Error 2
make[3]: Leaving directory '/usa/lisanhu/workspace/nvhpc-bugs/deep-data-access-NVVM_ERROR_COMPILATION/build'
CMakeFiles/Makefile2:80: recipe for target 'CMakeFiles/bug-cc.dir/all' failed
make[2]: *** [CMakeFiles/bug-cc.dir/all] Error 2
make[2]: Leaving directory '/usa/lisanhu/workspace/nvhpc-bugs/deep-data-access-NVVM_ERROR_COMPILATION/build'
CMakeFiles/Makefile2:87: recipe for target 'CMakeFiles/bug-cc.dir/rule' failed
make[1]: *** [CMakeFiles/bug-cc.dir/rule] Error 2
make[1]: Leaving directory '/usa/lisanhu/workspace/nvhpc-bugs/deep-data-access-NVVM_ERROR_COMPILATION/build'
Makefile:121: recipe for target 'bug-cc' failed
make: *** [bug-cc] Error 2
(base) lisanhu@skywalker:~/workspace/nvhpc-bugs/deep-data-access-NVVM_ERROR_COMPILATION/build
$ make bug-c
/usa/lisanhu/mine/libs/cmake/bin/cmake -S/usa/lisanhu/workspace/nvhpc-bugs/deep-data-access-NVVM_ERROR_COMPILATION -B/usa/lisanhu/workspace/nvhpc-bugs/deep-data-access-NVVM_ERROR_COMPILATION/build --check-build-system CMakeFiles/Makefile.cmake 0
make -f CMakeFiles/Makefile2 bug-c
make[1]: Entering directory '/usa/lisanhu/workspace/nvhpc-bugs/deep-data-access-NVVM_ERROR_COMPILATION/build'
/usa/lisanhu/mine/libs/cmake/bin/cmake -S/usa/lisanhu/workspace/nvhpc-bugs/deep-data-access-NVVM_ERROR_COMPILATION -B/usa/lisanhu/workspace/nvhpc-bugs/deep-data-access-NVVM_ERROR_COMPILATION/build --check-build-system CMakeFiles/Makefile.cmake 0
/usa/lisanhu/mine/libs/cmake/bin/cmake -E cmake_progress_start /usa/lisanhu/workspace/nvhpc-bugs/deep-data-access-NVVM_ERROR_COMPILATION/build/CMakeFiles 2
make -f CMakeFiles/Makefile2 CMakeFiles/bug-c.dir/all
make[2]: Entering directory '/usa/lisanhu/workspace/nvhpc-bugs/deep-data-access-NVVM_ERROR_COMPILATION/build'
make -f CMakeFiles/bug-c.dir/build.make CMakeFiles/bug-c.dir/depend
make[3]: Entering directory '/usa/lisanhu/workspace/nvhpc-bugs/deep-data-access-NVVM_ERROR_COMPILATION/build'
cd /usa/lisanhu/workspace/nvhpc-bugs/deep-data-access-NVVM_ERROR_COMPILATION/build && /usa/lisanhu/mine/libs/cmake/bin/cmake -E cmake_depends "Unix Makefiles" /usa/lisanhu/workspace/nvhpc-bugs/deep-data-access-NVVM_ERROR_COMPILATION /usa/lisanhu/workspace/nvhpc-bugs/deep-data-access-NVVM_ERROR_COMPILATION /usa/lisanhu/workspace/nvhpc-bugs/deep-data-access-NVVM_ERROR_COMPILATION/build /usa/lisanhu/workspace/nvhpc-bugs/deep-data-access-NVVM_ERROR_COMPILATION/build /usa/lisanhu/workspace/nvhpc-bugs/deep-data-access-NVVM_ERROR_COMPILATION/build/CMakeFiles/bug-c.dir/DependInfo.cmake --color=
Scanning dependencies of target bug-c
make[3]: Leaving directory '/usa/lisanhu/workspace/nvhpc-bugs/deep-data-access-NVVM_ERROR_COMPILATION/build'
make -f CMakeFiles/bug-c.dir/build.make CMakeFiles/bug-c.dir/build
make[3]: Entering directory '/usa/lisanhu/workspace/nvhpc-bugs/deep-data-access-NVVM_ERROR_COMPILATION/build'
[ 50%] Building C object CMakeFiles/bug-c.dir/bug.c.o
/opt/nvidia/hpc/20.7/Linux_x86_64/20.7/compilers/bin/nvc   -g -O0   -fPIC -acc -Minline -Minfo=accel -gpu=nordc -o CMakeFiles/bug-c.dir/bug.c.o   -c /usa/lisanhu/workspace/nvhpc-bugs/deep-data-access-NVVM_ERROR_COMPILATION/bug.c
main:
     12, Generating copyin(refs[:1]) [if not already present]
         Generating Tesla code
         15, #pragma acc loop gang /* blockIdx.x threadIdx.x */
nvvmCompileProgram error 9: NVVM_ERROR_COMPILATION.
Error: /tmp/pgaccJ3LhPTbitjvY.gpu (202, 10): parse stored value and pointer type do not match
ptxas /tmp/pgacct3Lh5WkDdtWL.ptx, line 1; fatal   : Missing .version directive at start of file '/tmp/pgacct3Lh5WkDdtWL.ptx'
ptxas fatal   : Ptx assembly aborted due to errors
NVC++-F-0155-Compiler failed to translate accelerator region (see -Minfo messages): Device compiler exited with error status code (/usa/lisanhu/workspace/nvhpc-bugs/deep-data-access-NVVM_ERROR_COMPILATION/bug.c: 12)
NVC++/x86-64 Linux 20.7-0: compilation aborted
CMakeFiles/bug-c.dir/build.make:65: recipe for target 'CMakeFiles/bug-c.dir/bug.c.o' failed
make[3]: *** [CMakeFiles/bug-c.dir/bug.c.o] Error 2
make[3]: Leaving directory '/usa/lisanhu/workspace/nvhpc-bugs/deep-data-access-NVVM_ERROR_COMPILATION/build'
CMakeFiles/Makefile2:107: recipe for target 'CMakeFiles/bug-c.dir/all' failed
make[2]: *** [CMakeFiles/bug-c.dir/all] Error 2
make[2]: Leaving directory '/usa/lisanhu/workspace/nvhpc-bugs/deep-data-access-NVVM_ERROR_COMPILATION/build'
CMakeFiles/Makefile2:114: recipe for target 'CMakeFiles/bug-c.dir/rule' failed
make[1]: *** [CMakeFiles/bug-c.dir/rule] Error 2
make[1]: Leaving directory '/usa/lisanhu/workspace/nvhpc-bugs/deep-data-access-NVVM_ERROR_COMPILATION/build'
Makefile:134: recipe for target 'bug-c' failed
make: *** [bug-c] Error 2
(base) lisanhu@skywalker:~/workspace/nvhpc-bugs/deep-data-access-NVVM_ERROR_COMPILATION/build
$ 
```
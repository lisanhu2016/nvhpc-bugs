#include <cstdio>

typedef struct mstring_t {
    size_t l;
    char *s;
} mstring_t;

int main() {
    mstring_t *refs = new mstring_t[1];
    refs[0].s = new char[10];
    refs[0].s[0] = 'a';

#pragma acc parallel loop copyin(refs[:1])
    for (size_t i = 0; i < 1; i++) {
        printf("refs[0].s[0] = %c\n", refs[i].s[0]);
    }

    delete[] refs[0].s;
    delete[] refs;
    return 0;
}

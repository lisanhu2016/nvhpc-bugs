#include "called.hpp"
#include <openacc.h>

int called(int *data, size_t length) {
    int sum = 0;

    // auto test = acc_hostptr(nullptr);
    auto d_data = acc_malloc(length * sizeof(int));
#pragma acc parallel loop reduction(+ : sum)
    for (size_t i = 0; i < length; i++) {
        sum += data[i];
    }
    acc_free(d_data);
    return sum;
}

#   Introduction

This bug happens when trying to link an OpenACC library compiled using nordc (unless you are compiling with nvhpc 20.7 with OpenACC flags)

##  Steps to reproduce
```
make
```

##  Observations
* If library is compiled in GPU mode, linking with GCC and NVHPC will both produce seg fault.
* If library is compiled in CPU mode, linking with GCC will produce seg fault.

##  System information
* OS: Ubuntu 18.04
* gcc: 7.5.0
* nvhpc: 20.7
* CUDA: 11.0

##  Console output
```bash
(base) lisanhu@skywalker:~/tmp/openaccrt_in_so
$ make
g++ -O3 driver.cc libcalled-gpu.a -o main-gpu -L/opt/nvidia/hpc/20.7/Linux_x86_64/20.7/compilers/lib -lacchost -laccdevice -lnvhpcman -ldl -lcudadevice -latomic -lnvhpcatm -lstdc++ -lnvomp -ldl -lnvhpcatm -latomic -lpthread -lnvcpumath -lnsnvc -lnvc -lm -lgcc -lc -lgcc -lgcc_s -lnvcpumath
g++ -O3 driver.cc libcalled-cpu.a -o main-cpu -L/opt/nvidia/hpc/20.7/Linux_x86_64/20.7/compilers/lib -lacchost -laccdevice -lnvhpcman -ldl -lcudadevice -latomic -lnvhpcatm -lstdc++ -lnvomp -ldl -lnvhpcatm -latomic -lpthread -lnvcpumath -lnsnvc -lnvc -lm -lgcc -lc -lgcc -lgcc_s -lnvcpumath
nvc++ -O3 driver.cc libcalled-cpu.a -o main-nvcpu
driver.cc:
nvc++ -O3 -acc -gpu=nordc -Minfo=accel driver.cc libcalled-gpu.a -o main-nvgpu
driver.cc:
(base) lisanhu@skywalker:~/tmp/openaccrt_in_so
$ ls
called-cpu.cc  called.hpp  driver.o     libcalled-cpu.a  main-cpu  main-nvcpu  Makefile
called-gpu.cc  driver.cc   libcalled.a  libcalled-gpu.a  main-gpu  main-nvgpu
(base) lisanhu@skywalker:~/tmp/openaccrt_in_so
$ ./main-nvcpu 
CPU sum: 1309579395
GPU sum: 1309579395
(base) lisanhu@skywalker:~/tmp/openaccrt_in_so
$ ./main-nvgpu
CPU sum: 1309579395
Segmentation fault (core dumped)
(base) lisanhu@skywalker:~/tmp/openaccrt_in_so
$ ./main-cpu
CPU sum: 1309579395
Segmentation fault (core dumped)
(base) lisanhu@skywalker:~/tmp/openaccrt_in_so
$ ./main-gpu
CPU sum: 1309579395
Segmentation fault (core dumped)
```
#include <cstddef>
#if !defined(CALLED)
#define CALLED

int called(int *data, size_t length);

#endif // CALLED

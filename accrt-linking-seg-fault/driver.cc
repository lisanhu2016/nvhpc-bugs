#include <c++/7/bits/c++config.h>
#include <cstdlib>
#include <iostream>
#include "called.hpp"

using namespace std;

int sum(int *data, size_t length) {
    int sum = 0;
    for (size_t i = 0; i < length; ++i) {
        sum += data[i];
    }
    return sum;
}

int main(int argc, const char *argv[]) {
    const size_t length = 1000000;
    int *data = static_cast<int *>(calloc(length, sizeof(int)));

    for (size_t i = 0; i < length; i++) {
        data[i] = rand();
    }

    printf("CPU sum: %d\n", sum(data, length));
    printf("GPU sum: %d\n", called(data, length));

    return 0;
}

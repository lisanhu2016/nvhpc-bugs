#   NVHPC Bugs

##  Introduction
This repo consists of samples of some known bugs in NVHPC. Making it easier to request for help. Probably will include workarounds for certain bugs.

## List of Bugs
1. [accrt-linking-seg-fault](accrt-linking-seg-fault)
   - OpenACC RT function calls will cause segment fault when the code is compiled with nordc and is linked to other driver code.
2. [deep-data-access-NVVM_ERROR_COMPILATION-bug](deep-data-access-NVVM_ERROR_COMPILATION)
   - NVHPC/20.7 could not compile this very simple code with `-acc`
